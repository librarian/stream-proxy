use actix_web::{error, web, Error, HttpResponse};
use awc::Client;

pub async fn forward_live(
  payload: web::Payload,
  path: web::Path<String>,
) -> Result<HttpResponse, Error> {
  let cdn_url = "https://cloud.odysee.live/";
  let url = cdn_url.to_owned() + &path;

  let client = Client::default();

  let forwarded_req = client
    .get(url)
    .insert_header((
      "User-Agent",
      "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0",
    ))
    .insert_header(("Origin", "https://odysee.com"))
    .insert_header(("Referer", "https://odysee.com/"))
    .no_decompress();

  let res = forwarded_req
    .send_stream(payload)
    .await
    .map_err(error::ErrorInternalServerError)?;

  let mut client_resp = HttpResponse::build(res.status());

  match res.headers().get("Content-Type") {
    None => client_resp.insert_header(("Content-Type", "application/octet-stream")),
    Some(header) => client_resp.insert_header(("Content-Type", header)),
  };
  client_resp.insert_header(("Access-Control-Allow-Origin", "*"));

  Ok(client_resp.streaming(res))
}
