use actix_web::{error, web, Error, HttpResponse};
use lazy_static::lazy_static;
use regex::Regex;
use bytes::{Bytes};

fn replace_manifest_urls(text: &str) -> std::string::String {
  lazy_static! {
    static ref RE: Regex = Regex::new("(?m)^/([0-9]{3}|live)").unwrap();
  }
  let new_text = RE.replace_all(text, "/live$0");
  let replaced_text = new_text.replace("https://cloud.odysee.live", "/live");
  replaced_text.replace("https://cdn.odysee.live", "/live")
}

pub async fn forward_manifest(
  path: web::Path<String>,
) -> Result<HttpResponse, Error> {
  let cdn_url = "https://cloud.odysee.live/";
  let url = cdn_url.to_owned() + &path + ".m3u8";

  let client = awc::Client::default();
  let req = client
    .get(url)
    .insert_header((
      "User-Agent",
      "Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0",
    ))
    .insert_header(("Origin", "https://odysee.com"))
    .insert_header(("Referer", "https://odysee.com/"));

  let mut res = req.send()
    .await
    .map_err(error::ErrorInternalServerError)?;

  let mut client_resp = HttpResponse::build(res.status());
  let body: Bytes = res.body().await?;
  let body_vec = body.to_vec();
  let body_string = String::from_utf8(body_vec).unwrap();
  let new_body = replace_manifest_urls(&body_string);

  client_resp.insert_header(("Access-Control-Allow-Origin", "*"));

  Ok(client_resp.body(new_body))
}
