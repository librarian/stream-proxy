mod live;
mod stream;
mod manifest;

use std::env;
use actix_web::{web, App, HttpServer};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
  let address = match env::var("ADDRESS") {
    Ok(val) => val,
    Err(_e) => "0.0.0.0".to_string(),
  };
  let port = match env::var("PORT") {
    Ok(val) => val,
    Err(_e) => "3001".to_string(),
  };

  HttpServer::new(move || {
    App::new()
      .route("/live/{path:.*}.m3u8", web::get().to(manifest::forward_manifest))
      .route("/live/{path:.*}", web::get().to(live::forward_live))
      .route("/stream/{path:.*}", web::get().to(stream::forward_stream))
  })
  .bind(address + &":".to_string() + &port)?
  .run()
  .await
}
