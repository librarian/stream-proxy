FROM lukemathwalker/cargo-chef:latest-rust-alpine AS chef
WORKDIR /src

FROM chef AS planner
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

FROM chef AS builder
COPY --from=planner /src/recipe.json recipe.json
RUN cargo chef cook --release --recipe-path recipe.json
COPY . .
ENV CARGO_INCREMENTAL=0
RUN cargo build --release --bin stream-proxy

FROM alpine:latest as bin

WORKDIR /app
COPY --from=builder /src/target/release/stream-proxy .

EXPOSE 3001

CMD ["/app/stream-proxy"]