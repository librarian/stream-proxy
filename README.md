> ⚠️ **ARCHIVED:** Use [stream-proxy-ng](https://codeberg.org/librarian/stream-proxy-ng) (will be integrated soon)

# stream-proxy
A streaming proxy for livestreams and videos for [Librarian](https://codeberg.org/librarian/librarian).

## Usage
stream-proxy should be run seperately on another domain using a reverse proxy such as [Caddy](https://caddyserver.com/).

### Docker (recommended)
Add the following to your `docker-compose.yml` file. Images for arm64 are currently unavailable, follow the instructions to build from source.
```
  stream-proxy:
    image: registry.gitlab.com/overtime-zone-wildfowl/stream-proxy:latest
    ports:
      - 3001:3001
    restart: unless-stopped
```

To start, run `sudo docker-compose up -d`. Enable in Librarian by adding the following to your `config.yml` file, then restart Librarian:
```
ENABLE_LIVE_STREAM: true
LIVE_STREAMING_URL: 'https://your.domain.tld/live'
```

### Build from source
You will need to install [Rust](https://rustup.rs/). Then run the following commands:
```sh
git clone https://codeberg.org/librarian/stream-proxy
cd stream-proxy
```

To build:
```sh
cargo build --release
```

The output will be at `target/release/stream-proxy` and can be run with the command `./target/release/stream-proxy`.

Enable in Librarian by adding the following to your `config.yml` file, then restart Librarian:
```
ENABLE_LIVE_STREAM: true
LIVE_STREAMING_URL: 'https://your.domain.tld/live'
```